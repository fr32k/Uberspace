#!/usr/bin/env bash

## info@fr32k.de 2020
#
## Dieses Skript zeigt dir eine Übersicht des
## Speicherplatzes auf deinem Uberspace an.

# Anzeige des verfügbaren Speicherplatzes
echo
quota -gsl
echo

# Anzeige des verfügbaren Speicherplatzes (erweitert)
echo
quotas=( $( quota -gl | grep dev | tr -s ' ' ' ' | sed 's/M//g' ) )

blocks=${quotas[1]}
free=$(( ${quotas[2]}-${quotas[1]} ))
quota=${quotas[2]}
limit=${quotas[3]}

echo -e "Disk quotas for \e[0;32m${USER}\e[0m:"
echo "Space: $blocks MB ($(( $blocks*100/$quota )) %)"
echo -e "\e[0;32mFree: $free MB ($(( $free*100/$quota )) %)\e[0m"
echo "Quota: $quota MB (100 %)"
echo -e "\e[0;31mLimit: $limit MB ($(( $limit*100/$quota )) %)\e[0m"

# Überprüfung, ob das Limit überschritten wurde
if [ $blocks -gt $quota ]; then
echo
echo -e "\e[1;31mHINT: You are over the quota!\e[0m"
echo "####: Clean up *at least* $(( $blocks-$quota )) MB of files!"
echo
else
echo
fi