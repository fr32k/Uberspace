#!/usr/bin/env bash
# src: https://lab.uberspace.de/guide_nextcloud.html#updates
# info@fr32k.de 2020

# please edit Nextcloud HOME
# ex. /home/$USER/html (root) or /home/$USER/html/cloud (subdir)
NC="/home/$USER/html/cloud"

###########################################################################
###########################################################################
# Other options
echo -e "Other Options:
  blank                normal updater mode
  --all                normal + db, selinux
  Maintenance and optimisations ONLY
  --db                 database optimisations
  --apps_update        apps update
  --selinux            default SELinux security of Nextcloud file(s)
  --off                force maintenance mode OFF
  --on                 force maintenance mode ON"
###########################################################################
DONE="echo -e done"
#
## Updater automatically works in maintenance:mode.
## Use the Uberspace backup system for files and database if you need to roll back.
## The Nextcloud updater creates backups only to safe base and app code data and config files
## so it takes ressources you might need for your productive data.
## Deactivate NC-updater Backups with --no-backup (works from 19.0.4, 18.0.10 and 17.0.10)
if [ "${1}" = "--on" ] || [ "${1}" = "--off" ] || [ "${1}" = "--selinux" ]  || [ "${1}" = "--apps_update" ]  || [ "${1}" = "--db" ]; then
  echo -e "Skip updater... Maintenance and optimisation only"
else
  php $NC/updater/updater.phar -vv --no-backup --no-interaction
fi

## maintenance mode ON
if [ "${1}" = "--on" ] || [ "${1}" = "--all" ] || [ "${1}" = "" ]; then
  php $NC/occ maintenance:mode --on
fi

## database optimisations
## The following command works from Nextcloud 20.
if [ "${1}" = "--db" ] || [ "${1}" = "--all" ]; then
  php $NC/occ db:add-missing-primary-keys --no-interaction
  ## The following command works from Nextcloud 19.
  php $NC/occ db:add-missing-columns --no-interaction
  php $NC/occ db:add-missing-indices --no-interaction
  php $NC/occ db:convert-filecache-bigint --no-interaction
fi
## apps update
if [ "${1}" = "--apps_update" ] || [ "${1}" = "--all" ] || [ "${1}" = "" ]; then
  echo -e "Starting apps update"
  php $NC/occ app:update --all && $DONE
fi

## maintenance mode OFF
if [ "${1}" = "--off" ] || [ "${1}" = "--all" ] || [ "${1}" = "" ]; then
  php $NC/occ maintenance:mode --off
fi

## default SELinux security of Nextcloud file(s)
if [ "${1}" = "--selinux" ] || [ "${1}" = "--all" ]; then
  echo -e "Starting restore file(s) default SELinux security contexts take time..."
  restorecon -R $NC && $DONE
fi